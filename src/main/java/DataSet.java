import java.util.*;

public class DataSet implements Iterable<Data>{
    List<Data> dataList;

    public DataSet(){
        dataList = new ArrayList<>();
    }

    public void addData(Data data){
        dataList.add(data);
    }

    public void sort(){
        dataList.sort((Comparator.comparing(Data::getName)));
    }

    @Override
    public Iterator<Data> iterator() {
        return dataList.iterator();
    }
}
