import java.util.*;
import java.util.stream.Collectors;

public class Service {
    public static List<Data> filterByName(List<Data> list, String name){
        return list.stream().filter(Objects::nonNull).filter(data -> data.getName().equals(name)).collect(Collectors.toList());
    }

    public static List<Data> filterByLevel(List<Data> list, double level){
        return list.stream().filter(Objects::nonNull).filter(data1 -> Math.abs(data1.getValue()) <= level ).collect(Collectors.toList());
    }

    public static Set<Double> filterByNameRepresentedValueSet(List<Data> list, Set<String> names){
        return list.stream().filter(Objects::nonNull).filter(data -> names.contains(data.getName())).
                mapToDouble(Data::getValue).collect(HashSet::new, HashSet::add, HashSet::addAll);
    }

    public static String[] filterPositiveValueRepresentedStringSet(List<Data> list){
        Set<String> res = new HashSet<>();
        List<Data> temp = list.stream().filter(Objects::nonNull).filter(data -> data.getValue() > 0).collect(Collectors.toList());
        for (Data d: temp){
            res.add(d.getName());
        }
        return (String[]) res.toArray();
    }

    public static<T> Set<T> intersection(List<Set<T>> setList){
        Set<T> res = setList.get(0);
        for(Set<T> set: setList){
            res.retainAll(set);
        }
        return res;
    }

    public static<T> Set<T> union(List<Set<T>> setList){
        Set<T> res = new HashSet<>();
        for (Set<T> set: setList){
            Collections.addAll(res, (T) set.toArray());
        }
        return res;
    }

    public static<T> List<Set<T>> getMaxSizeSets(List<Set<T>> setList){
        int maxSize = 0;
        for (Set<T> set: setList){
            if (set.size() > maxSize){
                maxSize = set.size();
            }
        }
        int finalMaxSize = maxSize;
        return setList.stream().filter(setList1 -> setList1.size() == finalMaxSize).collect(Collectors.toList());
    }
}
